import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class EcranPrincipal extends JPanel {

	private JFrame fenetre = new JFrame();
	private JLabel titre = new JLabel();
	private JLabel date = new JLabel();
	private JPanel appel = new JPanel();
	private JLabel groupeLabel = new JLabel();
	private JButton changerDate = new JButton();
	private JButton quitter = new JButton();
	private Groupe groupe = new Groupe();
	static private String jour = Integer.toString(Calendar.getInstance().get(
			Calendar.DAY_OF_MONTH));
	static private String mois = Integer.toString(Calendar.getInstance().get(
			Calendar.MONTH) + 1);
	static private String annee = Integer.toString(Calendar.getInstance().get(
			Calendar.YEAR));

	public EcranPrincipal(int entier, int intJour, int intMois, int intAnnee) {

		ArrayList<Etudiant> l1 = new ArrayList<Etudiant>();
		ArrayList<Etudiant> l2 = new ArrayList<Etudiant>();
		ArrayList<Etudiant> l3 = new ArrayList<Etudiant>();

		Etudiant e1 = new Etudiant("Gerard", "Cyril");
		Etudiant e2 = new Etudiant("Alder", "Hugo");
		Etudiant e3 = new Etudiant("Bernard", "Thibaut");
		Etudiant e4 = new Etudiant("Delbart", "Jonathan");
		Etudiant e5 = new Etudiant("Svevi", "Aurelien");

		l1.add(e1);
		l1.add(e2);
		l1.add(e3);
		l1.add(e4);
		l1.add(e5);

		Etudiant e6 = new Etudiant("Drogba", "Didier");
		Etudiant e7 = new Etudiant("Dupont", "Michel");
		Etudiant e8 = new Etudiant("Mankdin", "Spiration");
		Etudiant e9 = new Etudiant("Doe", "John");
		Etudiant e10 = new Etudiant("Jean", "Kevin");

		l2.add(e6);
		l2.add(e7);
		l2.add(e8);
		l2.add(e9);
		l2.add(e10);

		Etudiant e11 = new Etudiant("Jean", "Claude");
		Etudiant e12 = new Etudiant("Jean", "Kevin");
		Etudiant e13 = new Etudiant("Kevin", "Jean");
		Etudiant e14 = new Etudiant("Pascal", "Legranfrere");
		Etudiant e15 = new Etudiant("Jean", "Jean");

		l3.add(e11);
		l3.add(e12);
		l3.add(e13);
		l3.add(e14);
		l3.add(e15);

		Groupe h = new Groupe("H", l1);
		Groupe i = new Groupe("I", l2);
		Groupe j = new Groupe("J", l3);

		switch (entier) {
		case 1:
			setGroupe(h);
			break;
		case 2:
			setGroupe(i);
			break;
		default:
			setGroupe(j);
		}

		if (intJour != 0) {
			setJour(intJour);
		}

		if (intMois != 0) {
			setMois(intMois);
		}

		if (intAnnee != 0) {
			setAnnee(intAnnee);
		}

		fenetre.setTitle("AbsApp");
		fenetre.setPreferredSize(new Dimension(320, 480));
		fenetre.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		fenetre.setResizable(false);
		setBackground(new java.awt.Color(224, 224, 224));

		titre.setText("AbsApp");
		titre.setBackground(new java.awt.Color(0, 128, 255));
		titre.setFont(new java.awt.Font("L M Roman Caps10", 1, 25));
		titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		titre.setBounds(0, 0, 320, 60);
		titre.setOpaque(true);

		groupeLabel.setText("Groupe : " + getGroupe().getNom());
		groupeLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		groupeLabel.setBounds(0, 65, 320, 30);
		groupeLabel.setOpaque(true);

		Calendar calendrier = Calendar.getInstance();
		date.setText("Date : " + jour + " / " + mois + " / " + annee + "      "
				+ calendrier.get(Calendar.HOUR_OF_DAY) + " : "
				+ calendrier.get(Calendar.MINUTE));
		date.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		date.setBounds(0, 360, 320, 60);

		changerDate.setOpaque(true);
		changerDate.setText("Modifier");
		changerDate.setFont(new java.awt.Font("L M Roman Caps10", 1, 12));
		changerDate.setBounds(10, 420, 140, 30);
		changerDate.setBackground(java.awt.Color.WHITE);
		changerDate.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent evt) {
				fenetre.dispose();
				javax.swing.SwingUtilities.invokeLater(new Runnable() {

					public void run() {
						new Modifier();
					}
				});
			}
		});

		quitter.setOpaque(true);
		quitter.setText("Quitter");
		quitter.setFont(new java.awt.Font("L M Roman Caps10", 1, 12));
		quitter.setBounds(170, 420, 140, 30);
		quitter.setBackground(java.awt.Color.WHITE);
		quitter.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent evt) {
				fenetre.dispose();
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new Accueil();
					}
				});
			}
		});

		BoxLayout b = new BoxLayout(appel, BoxLayout.Y_AXIS);
		appel.setLayout(b);

		appel.setBackground(Color.WHITE);
		appel.setBounds(0, 100, 285, 260);

		for (Etudiant e : getGroupe().getEtudiants()) {

			BoutonAppel bouton = new BoutonAppel(e);

			Commentaire a = new Commentaire(e);
			a.setSize(new Dimension(280, 40));

			appel.add(bouton);
			appel.add(a);
		}

		JScrollPane p = new JScrollPane(appel,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		p.setBounds(10, 100, 301, 260);
		p.setVisible(true);
		fenetre.getContentPane().add(p);

		this.setLayout(null);
		this.add(titre);
		this.add(groupeLabel);
		this.add(changerDate);
		this.add(quitter);
		this.add(date);
		this.add(p);

		GroupLayout layout = new GroupLayout(fenetre.getContentPane());
		fenetre.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 999, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE));

		fenetre.pack();
		fenetre.setLocationRelativeTo(null);
		fenetre.setVisible(true);
	}

	public void setGroupe(Groupe g) {
		groupe = g;
	}

	public Groupe getGroupe() {
		return groupe;
	}

	public void setJour(int i) {
		jour = Integer.toString(i);
	}

	public void setMois(int i) {
		mois = Integer.toString(i);
	}

	public void setAnnee(int i) {
		annee = Integer.toString(i);
	}
}
