import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Accueil extends JPanel {

	private Identifiants identifiants = new Identifiants();
	private JFrame fenetre = new JFrame();
	private JLabel titre = new JLabel();
	private JLabel loginLabel = new JLabel();
	private JTextField login = new javax.swing.JTextField();
	private JLabel pswdLabel = new JLabel();
	private JPasswordField password = new JPasswordField();
	private JButton connectButton = new JButton();
	private GroupLayout layout = new GroupLayout(fenetre.getContentPane());

	public Accueil() {

		identifiants.ajouterProf();

		fenetre.setTitle("AbsApp");
		fenetre.setPreferredSize(new Dimension(320, 480));
		fenetre.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		fenetre.setResizable(false);
		setBackground(new java.awt.Color(224, 224, 224));

		titre.setText("AbsApp");
		titre.setBackground(new java.awt.Color(0, 128, 255));
		titre.setFont(new java.awt.Font("L M Roman Caps10", 1, 25));
		titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		titre.setBounds(0, 0, 320, 60);
		titre.setOpaque(true);

		loginLabel.setFont(new java.awt.Font("L M Roman Caps10", 1, 17));
		loginLabel.setText("Login : ");
		loginLabel.setBounds(20, 150, 75, 20);

		login.setBounds(160, 150, 130, 25);

		pswdLabel.setFont(new java.awt.Font("L M Roman Caps10", 1, 17));
		pswdLabel.setText("Password : ");
		pswdLabel.setBounds(20, 230, 120, 20);

		password.setBounds(160, 230, 130, 25);

		connectButton.setOpaque(true);
		connectButton.setText("Connect");
		connectButton.setFont(new java.awt.Font("L M Roman Caps10", 1, 12));
		connectButton.setBounds(80, 320, 160, 40);
		connectButton.setBackground(java.awt.Color.WHITE);
		connectButton.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent evt) {

				/*if (identifiants.getLogin().containsKey(login.getText())) {
					
					String mdp = identifiants.getLogin().get(login.getText());
					
					if (mdp.equals(password.getText())) {*/
						
						fenetre.dispose();
						javax.swing.SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								new EcranPrincipal(1,0,0,0);
							}
						});
					}
				//}
			//}
		});

		this.setLayout(null);
		this.add(titre);
		this.add(loginLabel);
		this.add(login);
		this.add(pswdLabel);
		this.add(password);
		this.add(connectButton);

		fenetre.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 999, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE));

		fenetre.pack();
		fenetre.setLocationRelativeTo(null);
		fenetre.setVisible(true);

	}
}
