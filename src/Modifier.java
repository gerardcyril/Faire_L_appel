import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Modifier extends JPanel {

	private JFrame fenetre = new JFrame();
	private JLabel titre = new JLabel();
	private GroupLayout layout = new GroupLayout(fenetre.getContentPane());
	private JButton valider = new JButton();
	private JButton annuler = new JButton();
	private JComboBox<String> jours = new JComboBox<>();
	private JComboBox<String> mois = new JComboBox<>();
	private JComboBox<String> annees = new JComboBox<>();
	private JComboBox<String> groupes = new JComboBox<>();
	private JLabel joursLabel = new JLabel();
	private JLabel moisLabel = new JLabel();
	private JLabel anneesLabel = new JLabel();
	private JLabel groupeLabel = new JLabel();

	public Modifier() {

		fenetre.setTitle("AbsApp");
		fenetre.setPreferredSize(new Dimension(320, 480));
		fenetre.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		fenetre.setResizable(false);
		setBackground(new java.awt.Color(224, 224, 224));

		titre.setText("AbsApp");
		titre.setBackground(new java.awt.Color(0, 128, 255));
		titre.setFont(new java.awt.Font("L M Roman Caps10", 1, 25));
		titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		titre.setBounds(0, 0, 320, 60);
		titre.setOpaque(true);

		joursLabel.setBounds(20, 60, 280, 35);
		joursLabel.setText("Jour");
		joursLabel.setBackground(Color.CYAN);
		joursLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		joursLabel.setVisible(true);

		String[] days = new String[30];
		for (int i = 0; i < 30; i++) {
			days[i] = Integer.toString(i + 1);
		}
		jours = new JComboBox<>(days);
		jours.setVisible(true);
		jours.setBounds(20, 95, 280, 35);

		moisLabel.setBounds(20, 130, 280, 35);
		moisLabel.setText("Mois");
		moisLabel.setBackground(Color.CYAN);
		moisLabel.setVisible(true);

		String[] months = new String[] { "Janvier", "Fevrier", "Mars", "Avril",
				"Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre",
				"Novembre", "Decembre" };
		mois = new JComboBox<>(months);
		mois.setVisible(true);
		mois.setBounds(20, 165, 280, 35);

		anneesLabel.setBounds(20, 200, 280,35);
		anneesLabel.setText("Annee");
		anneesLabel.setBackground(Color.CYAN);
		anneesLabel.setVisible(true);

		String[] years = new String[] { "2013", "2014", "2015" };
		annees = new JComboBox<>(years);
		annees.setVisible(true);
		annees.setBounds(20, 235, 280, 35);
		
		groupeLabel.setBounds(20, 270, 280, 35);
		groupeLabel.setText("Groupe");
		groupeLabel.setBackground(Color.CYAN);
		groupeLabel.setVisible(true);
		
		String[] groups = new String[] {"H","I","J"};
		groupes = new JComboBox<>(groups);
		groupes.setVisible(true);
		groupes.setBounds(20, 305, 280, 35);

		annuler.setOpaque(true);
		annuler.setText("Annuler");
		annuler.setFont(new java.awt.Font("L M Roman Caps10", 1, 12));
		annuler.setBounds(10, 420, 140, 30);
		annuler.setBackground(java.awt.Color.WHITE);
		annuler.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent evt) {
				fenetre.dispose();
				javax.swing.SwingUtilities.invokeLater(new Runnable() {

					public void run() {
						new EcranPrincipal(1,0,0,0);
					}
				});
			}
		});

		valider.setOpaque(true);
		valider.setText("Valider");
		valider.setFont(new java.awt.Font("L M Roman Caps10", 1, 12));
		valider.setBounds(170, 420, 140, 30);
		valider.setBackground(java.awt.Color.WHITE);
		valider.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent evt) {
				fenetre.dispose();
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new EcranPrincipal(groupes.getSelectedIndex(),jours.getSelectedIndex()+1,mois.getSelectedIndex()+1,annees.getSelectedIndex()+2013);
					}
				});
			}
		});

		this.setLayout(null);
		this.add(titre);
		this.add(annuler);
		this.add(valider);
		this.add(jours);
		this.add(mois);
		this.add(annees);
		this.add(groupes);
		this.add(joursLabel);
		this.add(moisLabel);
		this.add(anneesLabel);
		this.add(groupeLabel);
		

		fenetre.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 999, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE));

		fenetre.pack();
		fenetre.setLocationRelativeTo(null);
		fenetre.setVisible(true);
	}
}
