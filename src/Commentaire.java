import java.awt.Dimension;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;

public class Commentaire extends JTextField {

	private Etudiant etudiant;

	public Commentaire(Etudiant e) {
		super();
		etudiant = e;
		
		if (!etudiant.getCommentaire().equals("")) {
			this.setText(etudiant.getCommentaire());
		}
		else {
			setText("Commentaires :");
		}

		addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				if (!etudiant.getCommentaire().equals("")) {
					setText(etudiant.getCommentaire());
				}
				else {
					setText("");
				}
			}

			public void focusLost(FocusEvent e) {
				if (!etudiant.getCommentaire().equals("")) {
					setText(etudiant.getCommentaire());
				}
				else {
					setText("Commentaires :");
				}
			}
		});

		addKeyListener(new KeyListener() {

			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					etudiant.setCommentaire(getText());			
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});

	}
}
