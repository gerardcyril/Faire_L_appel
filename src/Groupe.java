import java.util.ArrayList;

public class Groupe {

	private String nom;
	private ArrayList<Etudiant> etudiants = new ArrayList<Etudiant>();
	
	public Groupe() {
		
	}
	
	public Groupe(String nom, ArrayList<Etudiant> e) {
		this.nom = nom;
		etudiants = e;
	}
	
	public String getNom() {
		return nom;
	}
	
	public ArrayList<Etudiant> getEtudiants() {
		return etudiants;
	}
}
