public class Etudiant {

	private String nom;
	private String prenom;
	private String commentaire = "";
	
	public Etudiant() {
		
	}
	
	public Etudiant(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
	}
	
	public String getNom() {
		return nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public String getNote() {
		return commentaire;
	}
	
	public void setCommentaire(String s) {
		commentaire = s;
	}
	
	public String getCommentaire() {
		return commentaire;
	}
}
