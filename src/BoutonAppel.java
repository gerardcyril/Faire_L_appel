import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JButton;

public class BoutonAppel extends JButton {

	private Etudiant etudiant;
	private int absence = 0;

	public BoutonAppel(Etudiant e) {
		super();
		etudiant = e;
		this.setCouleur();
		String s = this.getEtat();
		this.setText(etudiant.getNom() + " " + etudiant.getPrenom() + " " + s);

		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				absence = absence + 1;
				String s = getEtat();
				setText(etudiant.getNom() + " " + etudiant.getPrenom() + " " + s);
				setCouleur();
			}
		});
		
		setSize(280,30);
		setBorderPainted(false);
		setFont(new java.awt.Font("Arial", 1, 14));
		setMinimumSize(new Dimension(290, 40));
		setMaximumSize(new Dimension(290,40));
		setPreferredSize(new Dimension(290, 40));
	}

	public void setCouleur() {
		switch (absence % 3) {
		case 0:
			setBackground(Color.CYAN); break;
		case 1:
			setBackground(Color.YELLOW); break;
		case 2:
			setBackground(Color.RED); break;
		}
	}
	
	public String getEtat() {
		switch (absence % 3) {
		case 1:
			return "En retard";
		case 2:
			return "Absent";
		}
		return "";
	}

}
